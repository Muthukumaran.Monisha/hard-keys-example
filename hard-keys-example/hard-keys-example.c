/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "hard-keys-example.h"

#include <mildenhall/mildenhall.h>

struct _HlwApp
{
  /*< private >*/
  GApplication parent;

  ClutterActor *stage;          /* owned */
};

G_DEFINE_TYPE (HlwApp, hlw_app, G_TYPE_APPLICATION);

static gboolean
on_stage_destroy (ClutterActor *stage,
                  gpointer user_data)
{
  GApplication *app = user_data;

  g_debug ("The main stage is destroyed.");

  /* chain up */
  CLUTTER_ACTOR_GET_CLASS (stage)->destroy (stage);

  /* The main window is detroyed (closed) so the application
   * needs to be released */
  g_application_release (app);

  return TRUE;
}

static void
on_key_release_cb (ClutterActor *actor,
                   ClutterEvent *event,
                   HlwApp *app)
{
  ClutterColor color;
  guint keysym = clutter_event_get_key_symbol (event);

  clutter_actor_get_background_color (app->stage, &color);

  /* See clutter/clutter-keysyms.h for all keys */
  switch (keysym)
  {
    case CLUTTER_KEY_q:
    case CLUTTER_KEY_Escape:
      g_application_quit (G_APPLICATION (app));
      break;
    case CLUTTER_KEY_Right:
    case CLUTTER_KEY_rightarrow:
    case CLUTTER_KEY_Forward:
      clutter_color_lighten (&color, &color);
      break;
    case CLUTTER_KEY_Left:
    case CLUTTER_KEY_leftarrow:
    case CLUTTER_KEY_Back:
      clutter_color_darken (&color, &color);
      break;
    default:
      break;
  }

  clutter_actor_set_background_color (app->stage, &color);
}

static void
startup (GApplication *app)
{
  HlwApp *self = HLW_APP (app);
  ClutterActor *text_label;

  g_application_hold (app);

  /* chain up */
  G_APPLICATION_CLASS (hlw_app_parent_class)->startup (app);

  /* build ui components */
  self->stage = mildenhall_stage_new (g_application_get_application_id (app));

  g_signal_connect (G_OBJECT (self->stage),
      "destroy", G_CALLBACK (on_stage_destroy),
      self);

  clutter_actor_set_layout_manager (self->stage,
      clutter_bin_layout_new (CLUTTER_BIN_ALIGNMENT_CENTER,
                              CLUTTER_BIN_ALIGNMENT_CENTER));


  clutter_actor_set_background_color (self->stage, CLUTTER_COLOR_DarkGray);

  text_label = clutter_text_new_full (
    "DejaVu Sans 20px",
    "Use Forward/Right and Left/Back to change brightness.",
    CLUTTER_COLOR_White);

  clutter_actor_add_child (self->stage, text_label);

  g_signal_connect (self->stage, "key-release-event",
                    G_CALLBACK (on_key_release_cb), self);

}

static void
activate (GApplication *app)
{
  HlwApp *self = HLW_APP (app);
  clutter_actor_show (self->stage);
}

static void
hlw_app_dispose (GObject *object)
{
  HlwApp *self = HLW_APP (object);

  g_clear_pointer (&self->stage, clutter_actor_destroy);

  G_OBJECT_CLASS (hlw_app_parent_class)->dispose (object);
}

static void
hlw_app_class_init (HlwAppClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->dispose = hlw_app_dispose;

  app_class->startup = startup;
  app_class->activate = activate;
}

static void
hlw_app_init (HlwApp *self)
{
}
